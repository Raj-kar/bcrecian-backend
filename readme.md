## Backend Repo for our [College Space](https://gitlab.com/Raj-kar/bcrecian)
&nbsp;

## Getting Started
First clone the project : 

If you already installed, Python >= 3.6 and virtualenv, then go to Next part.
If not, hold on !

First download and install python from [here](https://www.python.org/ftp/python/3.9.7/python-3.9.7-amd64.exe).
The Open ternimal / git bash window.

# Install Python
```bash
python Python -V
Python 3.9.6
```
##### If you can see something this, go to the next step, other wise just GooGLE it.

# Install & active virtualenv
```bash
pip install virtualenv
virtualenv <your_env_name>
source Scripts/active                    // (for active virtualenv)
```
# Clone Repo and install dependencies
```bash
git clone https://gitlab.com/Raj-kar/bcrecian-backend.git
cd bcrecian-backend
pip install -r requirements.txt         // (it will install all dependencies)
```
# For run the local development server :

```bash
python manage.py runserver
# or
py manage.py runserver
```
# Git CheatSheet
##### make sure, to create your own branch. 
##### Never edit or modify in master branch.
&nbsp;

## How to Create and switch to new branch ?
```bash
git checkout -b <your branch name>
```

## How to check all the branchs ?
```bash
git branch
```
## How to check git logs ?
```bash
git log
```

## How to add changes ?
```bash
git add .
```

## How to commit changes ?
```bash
git commit -m "your_commit_message"
```

## How to push your changes on Gitlab / Github?
```bash
git push -u origin <your_branch_name>
```

## How to pull (get) someones code?
```bash
git pull origin <the_branch_name_you_want_to_pull>
```

## How to merge your code with some other branches
```bash
git add .
git commit -m "your message"    
git push -u origin <your_branch_name>           (make sure you commit all your changes)

git checkout -b <the_branch_name_with_whom_you_want_to_merge>
git pull origin <the_branch_name_with_whom_you_want_to_merge>

git add .
git commit -m "your message"                   (make sure you commit the new changes)

git pull origin <your_branch_name>
git merge
```

### THAT's more than enough :)