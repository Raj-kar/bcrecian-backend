from rest_framework.response import Response
import json

from rest_framework.views import APIView

from paper.models import Course, Semester
from .models import Logbook
from .serializers import LogSerializer


class AllLogsView(APIView):
    def get(self, request):
        course = request.GET.get("course", "BCA")
        semester = request.GET.get("semester", 1)
        section = request.GET.get("section", "A")

        course = Course.objects.filter(slug=course)
        semester = Semester.objects.filter(sem_number=semester)

        if course and semester:
            logs = Logbook.objects.filter(
                course=course[0], semester=semester[0], section=section)
            serializer = LogSerializer(logs, many=True)

            if not serializer:
                return Response({"status": 200, 'message': "No Log Found, Add using the + button."})
            return Response({"status": 200, "payload": serializer.data})
        return Response({'status': 200, "message": "No Class record Found in DB !!"})

    def put(self, request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        ID = body['id']

        try:
            log = Logbook.objects.get(id=ID)
        except Exception as e:
            return Response({"messsage": "NO LOG FOUND !!", 'status': 200})
        else:
            log.signature = not log.signature
            log.save()
            return Response({"message": "Signature updated !", "status": 200})

    def delete(self, request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        ID = body['id']

        try:
            log = Logbook.objects.get(id=ID)
        except Exception as e:
            return Response({"messsage": "NO LOG FOUND !!", 'status': 200})
        else:
            log.signature = not log.signature
            log.delete()
            return Response({"message": "Log Deleted !", "status": 200})
