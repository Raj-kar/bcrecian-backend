from django.db import models

from paper.models import Course, Paper, Semester
from teacher.models import Teacher


class Logbook(models.Model):
    date = models.DateField(verbose_name="Current Date")
    start_time = models.TimeField()
    end_time = models.TimeField()
    paper_code = models.ForeignKey(Paper, on_delete=models.CASCADE)
    teacher_name = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    topics = models.TextField(verbose_name="Topics")
    signature = models.BooleanField(default=False)
    created_by = models.CharField(max_length=255)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
    section = models.CharField(max_length=1)

    def __str__(self) -> str:
        return f"{self.paper_code} - {self.teacher_name} | ( {self.start_time} - {self.end_time} ) "
