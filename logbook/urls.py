from django.urls import path
from .views import AllLogsView

urlpatterns = [
    path('logs/', AllLogsView.as_view(), name='All Logs'),
]
