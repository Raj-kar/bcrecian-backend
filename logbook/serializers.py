from rest_framework import serializers

from .models import Logbook
from teacher.serializers import TeacherSerializer


class LogSerializer(serializers.ModelSerializer):
    teacher_name = TeacherSerializer()

    class Meta:
        model = Logbook
        fields = ["id", "date", "start_time", "end_time",
                  "paper_code", "teacher_name", "topics", "signature", ]
