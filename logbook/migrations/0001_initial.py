# Generated by Django 3.2.6 on 2021-12-02 07:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('paper', '0002_alter_paper_credit'),
        ('teacher', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Logbook',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Current Date')),
                ('start_time', models.TimeField()),
                ('end_time', models.TimeField()),
                ('topics', models.TextField(verbose_name='Topics')),
                ('signature', models.BooleanField(default=False)),
                ('created_by', models.CharField(max_length=255)),
                ('section', models.CharField(max_length=1)),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='paper.course')),
                ('paper_code', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='paper.paper')),
                ('semester', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='paper.semester')),
                ('teacher_name', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='teacher.teacher')),
            ],
        ),
    ]
