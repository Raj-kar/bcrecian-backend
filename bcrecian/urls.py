from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("paper.urls")),
    path('', include("routine.urls")),
    path('', include("notes.urls")),
    path('', include("job.urls")),
    path('', include("logbook.urls")),
    # path('', include("classes.urls")),
    path('', include("notification.urls")),
    path('', include("helpers.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
