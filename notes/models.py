from django.db import models
from django.utils import timezone

from paper.models import Course, Paper, Semester


class Note(models.Model):
    SECTION_CHOICES = (
        ('A', 'A'),
        ('B', 'B')
    )

    course = models.ForeignKey(
        Course, on_delete=models.CASCADE, related_name='note_course')
    semester = models.ForeignKey(
        Semester, on_delete=models.CASCADE, related_name='note_semester')
    section = models.CharField(max_length=1, choices=SECTION_CHOICES, default='B')
    paper_code = models.ForeignKey(Paper, on_delete=models.CASCADE)
    file_link = models.URLField()
    note_date = models.DateField(default=timezone.now)
    upload_by = models.CharField(max_length=50, default='admin')
    upload_time = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return f"{self.semester} - {self.paper_code}"
