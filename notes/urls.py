from django.urls import path
from .views import Notes

urlpatterns = [
    path('allnotes/', Notes.as_view(), name='all notes'),
]
