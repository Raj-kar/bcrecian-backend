from django.contrib import admin

from notes.models import Note


class NoteAdmin(admin.ModelAdmin):
    list_display = ('course', 'semester', 'paper_code', 'upload_by')
    list_display_links = ('course', 'semester', 'paper_code')


admin.site.register(Note, NoteAdmin)
