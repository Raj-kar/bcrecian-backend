from rest_framework import serializers
from .models import Note
from paper.serializers import PaperSerializer


class NoteSerializer(serializers.ModelSerializer):
    paper_code = PaperSerializer()

    class Meta:
        model = Note
        fields = ['id', 'paper_code', 'note_date', 'file_link']
