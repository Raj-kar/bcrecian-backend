from datetime import datetime
import json
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Note
from .serializer import NoteSerializer
import collections

from paper.models import Course, Semester


class Notes(APIView):

    def refine_data(self, data):
        """Convert Notes array, into key value pair, where key is the date, and values are the notes."""
        newlist = list(data)
        newdict = {}
        for i in range(len(newlist)):
            # convert string date to datetime object
            datetime_obj = datetime.fromisoformat(newlist[i]['note_date'])
            newlist[i]['note_date'] = datetime_obj.strftime(
                "%d %b, %Y")
            if newlist[i]['note_date'] in newdict.keys():
                newdict[newlist[i]['note_date']].append(newlist[i])
            else:
                newdict[newlist[i]['note_date']] = []
                newdict[newlist[i]['note_date']].append(newlist[i])
        # No data found
        if not bool(newdict):
            return False

        sorted_dict = collections.OrderedDict(
            sorted(newdict.items(), reverse=True))
        return sorted_dict

    def get(self, request):

        course_name = request.GET.get("course", "BCA")
        course = Course.objects.filter(slug=course_name)
        if not course:
            return Response({'status': 422, 'message': 'No Course found, Contact with your CRs'})

        sem_number = request.GET.get("semester", 1)
        semester = Semester.objects.filter(
            course=course[0], sem_number=sem_number)
        if not semester:
            return Response({'status': 422, 'message': 'No Semester found, Contact with your CRs'})

        section = request.GET.get("section", "A")
        notes = Note.objects.filter(
            course=course[0], semester=semester[0], section=section)

        serializer = NoteSerializer(notes, many=True)
        result = self.refine_data(serializer.data)
        if not result:
            return Response({'status': 422, 'message': 'No Semester found, Contact with your CRs'})
        else:
            return Response({'status': 200, 'payload': result})

    def post(self, request):
        body = json.loads(request.body)

        course_name = body['course']
        course = Course.objects.filter(slug=course_name)
        if not course:
            return Response({'status': 422, 'message': 'No Course found, Contact with your CRs.'})

        sem_number = body['semester']
        semester = Semester.objects.filter(
            course=course[0], sem_number=sem_number)
        if not semester:
            return Response({'status': 422, 'message': 'No Notes found, Contact with your CRs.'})

        section = body['section']
        keyword = body['keyword']

        # search the keyword in notes
        notes = Note.objects.filter(
            course=course[0], semester=semester[0], section=section).filter(Q(section=section) and Q(paper_code__name__icontains=keyword) | Q(
                paper_code__code__icontains=keyword) | Q(note_date__icontains=keyword))

        serializer = NoteSerializer(notes, many=True)
        result = self.refine_data(serializer.data)
        if not result:
            return Response({'status': 422, 'message': 'No Notes found, Try with a different keyword'})
        else:
            return Response({'status': 200, 'payload': result})
