from django.db import models
from ckeditor.fields import RichTextField
from notification.models import Notification
from helpers.views import save_notification
import html2text
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.


class Tag(models.Model):
    slug = models.CharField(max_length=10, primary_key=True)

    def __str__(self) -> str:
        return self.slug


class Job(models.Model):
    title = models.CharField(max_length=50, null=True)
    ongoing = models.BooleanField(default=True)
    apply_link = models.URLField(
        verbose_name='Apply URL', help_text="Add direct registration URL")
    img_url = models.URLField()
    tags = models.ManyToManyField(Tag, related_name="Tag")
    body = RichTextField()

    def __str__(self) -> str:
        return self.title


@receiver(post_save, sender=Job)
def _job_save_receiver(sender, instance, created, **kwargs):
    data = {}
    data['title'] = 'New job Update! ' + instance.title
    if created:
        data['title'] = 'New Job! ' + instance.title

    data['id'] = instance.id
    data['body'] = instance.body
    data['target'] = [{}]
    try:
        data['target'].append({'course': instance.course})
    except Exception as e:
        pass
    try:
        data['target'].append({'semester': instance.semester})
    except Exception as e:
        pass
    try:
        data['target'].append({'section': instance.section})
    except Exception as e:
        pass
    _id = f"announcement{instance.id}"
    save_notification(_id, instance.title,
                      html2text.html2text(instance.body))
    # show_data(data)
