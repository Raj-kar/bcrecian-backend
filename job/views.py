from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from job.models import Job
from job.serializers import JobSerializers

# Create your views here.


class JobView(APIView):

    def get(self, request):
        jobs = Job.objects.all()

        serializer = JobSerializers(jobs, many=True)
        return Response(reversed(serializer.data), status=status.HTTP_200_OK)
