from django.contrib import admin
from django.urls import path
from .views import get_notifications, get_new_notification_count

urlpatterns = [
    path('get-notifications/', get_notifications),
    path('get-new-notification-count/', get_new_notification_count)
]
