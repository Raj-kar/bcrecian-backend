from notification.models import Notification
from notification.serializers import NotificationSerializer
import datetime

from rest_framework.response import Response
from rest_framework.decorators import api_view
import requests
import json
from django.views.decorators.csrf import csrf_exempt
from notification.serializers import NotificationSerializer
import datetime

from bcrecian.env import FRONTEND
# Create your views here.


def save_notification(id, title, body, course=None, semester=None, section=None):
    print(id)
    try:
        obj = Notification.objects.get(notification_id=id)
        if obj:
            obj.notification_title = title
            obj.notification_body = body
            obj.notification_target_semester = semester
            obj.notification_target_stream = course
            obj.notification_target_section = section
            obj.save()

    except Exception as e:
        new_notification = Notification(notification_id=id, notification_title=title, notification_body=body,
                                        notification_target_semester=semester, notification_target_stream=course, notification_target_section=section)
        new_notification.save()
    return 0


@api_view(['POST'])
def get_notifications(request):
    data = request.data
    print(data)
    notifications = Notification.objects.all()
    print(notifications)
    serializer = NotificationSerializer(notifications, many=True)
    notifications = serializer.data
    # print(notifications)
    filtered_dict = []
    for notification in notifications:
        try:
            if(not notification['notification_target_stream'] or notification['notification_target_stream'] == data['stream']):
                if(not notification['notification_target_semester'] or notification['notification_target_semester'] == data['semester']):
                    if(not notification['notification_target_section'] or notification['notification_target_section'] == data['section']):
                        if(not notification['notification_target_roll'] or notification['notification_target_roll'] == data['roll']):
                            print(notification['notification_date'])
                            datetime_obj = datetime.datetime.fromisoformat(
                                notification['notification_date'])
                            formated_date = datetime_obj.strftime(
                                "%H:%M:%S  - %d %b, %Y ")
                            new_obj = {"title": notification['notification_title'],
                                       "body": notification['notification_body'],
                                       "date": formated_date}

                            filtered_dict.append(new_obj)
        except Exception as e:
            print(e)
    try:
        url = f"{FRONTEND}/api/auth/lastnotificationcheck?email={data['email']}&changetime=true"
        res = requests.get(url)
        print(res.json())
        if(res.ok):
            return Response({'status': 200, 'payload': filtered_dict, 'message': "Data sended"})
        else:
            return Response({'status': 200, 'payload': [], 'message': "Data sended"})
    except Exception as e:
        print(e)
        return Response({'status': 200, 'payload': [], 'message': "Data sended"})


@api_view(['POST'])
def get_new_notification_count(request):
    count = 0
    try:
        body = request.data
        print(body)
        url = f"{FRONTEND}/api/auth/lastnotificationcheck?email={body['email']}&changetime=false"
        res = requests.get(url)
        print(res.json())
        if(res.ok):
            previous_check_time = (res.json())
            previous_check_time = str(previous_check_time['previous_time'])
            previous_check_time = datetime.datetime.strptime(
                previous_check_time, '%H:%M:%S %d %b,%Y').replace(tzinfo=datetime.timezone.utc)
            print(previous_check_time)
            notifications = list(Notification.objects.values())

            for notification in notifications:
                current_time = datetime.datetime.fromisoformat(
                    str(notification['notification_date']))
                localtime = datetime.datetime.strptime(
                    str(current_time), '%Y-%m-%d %H:%M:%S.%f+00:00')
                time_change = datetime.timedelta(hours=5, minutes=30)
                current_time = localtime+time_change
                current_time = current_time.replace(
                    tzinfo=datetime.timezone.utc)
                print(current_time)

                if(previous_check_time < current_time):
                    count += 1
            print(count)
            return Response(count)
        else:
            print('Failed to send data because of node api problem')
            return Response(count)
    except Exception as e:
        print(e)
        print('Failed to send data because of node api problem')
        return Response(count)
