from datetime import date, datetime, time

from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from .models import Holiday, TimeTable, Announcement
from paper.models import Course, Semester
from .serializers import TimeTableSerializer, AnnouncementSerializer


# request is passed for extract data and return the value
def isHoliday(request):
    # Check if today is any Holiday
    holiday = Holiday.objects.filter(date=datetime.today())
    if holiday:
        return {'is_holiday': True, 'response': {'status': 200, 'message': holiday[0].holiday_reason}}

    # Fetch day
    day = int(request.GET.get("day", -1))
    if day == -1:
        day = datetime.today().isoweekday()    

    if day in [1, 7]:
        return {'is_holiday': True, 'response': {'status': 200, 'message': '🎉 Weekly Holiday 🎉'}}

    if day > 7 or day < 1:
        return {'is_holiday': True, 'response': {'status': 200, 'message': 'I think you are drunk! 🎉'}}

    # Fetch request details.
    course = request.GET.get("course", "BCA")
    semester = request.GET.get("semester", 1)
    section = request.GET.get("section", "A")

    # Grab data from tables
    course = Course.objects.filter(slug=course)
    semester = Semester.objects.filter(
        course=course[0]).filter(sem_number=semester)

    return {'is_holiday': False, 'details': [day, course, semester, section]}


class TimeTableView(APIView):
    def get(self, request):
        response = isHoliday(request)

        if response['is_holiday']:
            return Response(response['response'])

        response = response['details']
        day, course, semester, section = response[0], response[1], response[2], response[3]

        # Check if course and semester presents in our database
        if course and semester:
            timetable = TimeTable.objects.filter(
                course=course[0], semester=semester[0], section=section, day=day)
            serializer = TimeTableSerializer(timetable, many=True)

            if len(serializer.data) > 0:
                return Response({'status': 200, 'payload': serializer.data})
            else:
                return Response({'status': 200, 'message': 'Timetable will be avaliable soon!'})

        else:
            return Response({'status': 403, 'message': 'No Timetable found !'})


class AnnouncementView(APIView):
    def get(self, request):
        announcement = Announcement.objects.all()

        if not announcement:
            return Response({'status': 403, 'message': 'No Announcement found !'})
        serializer = AnnouncementSerializer(announcement, many=True)
        for obj in serializer.data:
            datetime_obj = datetime.fromisoformat(obj['upload_date_time'])
            obj['upload_date_time'] = datetime_obj.strftime(
                "%d %b %Y, at %I:%M %p")

        return Response({'status': 200, 'payload': serializer.data})


class ClassDetials(APIView):
    def get(self, request):
        response = isHoliday(request)

        if response['is_holiday']:
            return Response(response['response'])

        response = response['details']
        day, course, semester, section = response[0], response[1], response[2], response[3]

        # Check if course and semester presents in our database
        if course and semester:
            timetable = TimeTable.objects.filter(
                course=course[0], semester=semester[0], section=section, day=day)
            class_start = timetable[0].start_time
            class_end = timetable[len(timetable)-1].end_time
            current_time = datetime.now().strftime('%H:%M:%S')

            if (current_time < str(class_start)):
                return Response({'message': "Classes are not started yet !!"})
            elif (current_time > str(class_end)):
                return Response({'message': "All Classes are end for today !!"})

            for each in timetable:
                if(self.isNowInTimePeriod(str(each.start_time), str(each.end_time), current_time)):
                    serializer = TimeTableSerializer(each)
                    return Response({'status': 200, 'payload': serializer.data})

            return Response({'message': 'Hmmn.. Seems like we lost on Space !!!'})
        else:
            return Response({'status': 403, 'message': 'No Data Found found !'}, status=status.HTTP_403_FORBIDDEN)

    def isNowInTimePeriod(self, startTime, endTime, nowTime):
        if startTime < endTime:
            return nowTime >= startTime and nowTime <= endTime
        else:
            # Over midnight:
            return nowTime >= startTime or nowTime <= endTime


class RoutineView(APIView):
    def get(self, request):
        response = isHoliday(request)

        if response['is_holiday']:
            return Response(response['response'])

        response = response['details']
        day, course, semester, section = response[0], response[1], response[2], response[3]

        if course and semester:
            routine = TimeTable.objects.filter(
                course=course[0], semester=semester[0], section=section, day=day)
            serializer = TimeTableSerializer(routine, many=True)

            if len(serializer.data) > 0:
                return Response({'status': 200, 'payload': serializer.data})
            else:
                return Response({'status': 200, 'message': 'Timetable will be avaliable soon!'})

        return Response({'status': 403, 'message': 'No Timetable found !'})
