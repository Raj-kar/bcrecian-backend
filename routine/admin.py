from django.contrib import admin

from routine.models import *


class TimeTableAdmin(admin.ModelAdmin):
    list_display = ('teacher', 'course', 'semester',
                    'paper', 'day', 'start_time', 'end_time', 'is_cancel')
    list_display_links = ('teacher', 'course', 'semester', 'paper', 'day')
    list_editable = ('is_cancel',)
    search_fields = ('teacher', 'course', 'semester',
                     'paper', 'day', 'start_time', 'end_time')
    

admin.site.register(Holiday)
admin.site.register(TimeTable, TimeTableAdmin)
admin.site.register(Announcement)