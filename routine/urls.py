from django.urls import path
from .views import *

urlpatterns = [
    path('timetable/', TimeTableView.as_view(), name='time-table'),
    path('announcements/', AnnouncementView.as_view(), name='announcement'),
    path('classdetails/', ClassDetials.as_view(), name='class_details'),
    path('routine/', RoutineView.as_view(), name='routine'),
]
