from rest_framework import serializers

from .models import TimeTable, Announcement
from paper.serializers import PaperSerializer
from teacher.serializers import TeacherSerializer


class TimeTableSerializer(serializers.ModelSerializer):
    paper = PaperSerializer()
    teacher = TeacherSerializer()

    class Meta:
        model = TimeTable
        fields = ('paper', 'teacher', 'start_time', 'end_time', 'is_cancel')


class AnnouncementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Announcement
        fields = ('title', 'body', 'upload_by', 'upload_date_time')
