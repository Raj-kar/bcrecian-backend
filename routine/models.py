from django.db import models
from ckeditor.fields import RichTextField

from paper.models import Course, Paper, Semester
from teacher.models import Teacher
from django.db.models.signals import post_save
from django.dispatch import receiver
import json
import requests
from notification.views import show_data


from paper.serializers import PaperSerializer
from teacher.serializers import TeacherSerializer
from rest_framework import serializers

from notification.models import Notification
from helpers.views import save_notification
import html2text


class Holiday(models.Model):
    date = models.DateField()
    holiday_reason = models.TextField()

    def __str__(self) -> str:
        return f"{self.date.strftime('%b %d')} - {self.holiday_reason}"


class TimeTable(models.Model):
    DAYS_CHOICES = (
        ('0', 'Sunday'),
        ('1', 'Monday'),
        ('2', 'Tuesday'),
        ('3', 'Wednesday'),
        ('4', 'Thursday'),
        ('5', 'Friday'),
        ('6', 'Saturday')
    )

    SECTION_CHOICES = (
        ('A', 'A'),
        ('B', 'B')
    )

    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    course = models.ForeignKey(
        Course, on_delete=models.CASCADE, related_name='time_table_course', default='BCA')
    semester = models.ForeignKey(
        Semester, on_delete=models.CASCADE, related_name='time_table_semester', default='5')
    section = models.CharField(
        max_length=1, choices=SECTION_CHOICES, default='B')
    paper = models.ForeignKey(Paper, on_delete=models.CASCADE)
    day = models.CharField(choices=DAYS_CHOICES, max_length=30)
    start_time = models.TimeField()
    end_time = models.TimeField()
    is_cancel = models.BooleanField(default=False)

    def __str__(self) -> str:
        return f"{self.paper} - {self.teacher}"


class Announcement(models.Model):
    title = models.CharField(
        help_text="Try to add title within 50 letters.", max_length=100)
    body = RichTextField()
    upload_by = models.CharField(default="admin", max_length=20)
    upload_date_time = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return f"{self.title} - {self.upload_date_time.strftime('%d %b, %Y')}"


@receiver(post_save, sender=Announcement)
def _announcement_save_receiver(sender, instance, created, **kwargs):
    data = {}
    data['title'] = 'Announcement Update! ' + instance.title
    if created:
        data['title'] = 'New announcement! ' + instance.title

    data['id'] = instance.id
    data['body'] = instance.body
    data['target'] = [{}]
    try:
        data['target'].append({'course': instance.course})
    except Exception as e:
        pass
    try:
        data['target'].append({'semester': instance.semester})
    except Exception as e:
        pass
    try:
        data['target'].append({'section': instance.section})
    except Exception as e:
        pass
    _id = f"announcement{instance.id}"
    save_notification(_id, instance.title,
                      html2text.html2text(instance.body))
    # show_data(data)


@receiver(post_save, sender=TimeTable)
def _time_table_save_receiver(sender, instance, created, **kwargs):
    if not created:
        data = {}
        serializer = TimeTableSerializer(instance)
        print(instance.is_cancel)
        print(serializer['semester'])
        course = ((serializer['semester']['course']).value)
        semester = ((serializer['semester']['sem_number']).value)
        section = (instance.section)
        data['id'] = instance
        data['title'] = 'Class Update!'
        if(instance.is_cancel):
            data['body'] = str(instance) + f"'s Class Canceled!"
        else:
            data['body'] = str(
                instance) + f"'s Class is not canceled!, It will be held on routine time"

        data['target'] = {}
        data['target']['course'] = course
        data['target']['semester'] = semester
        data['target']['section'] = section
        _id = f"timetable{instance.id}"
        save_notification(_id, data['title'], data['body'],
                          Course.objects.get(slug=course), semester, section)
        # show_data(data)


class TimeTableSerializer(serializers.ModelSerializer):
    paper = PaperSerializer()
    teacher = TeacherSerializer()

    class Meta:
        model = TimeTable
        #('paper', 'teacher', 'start_time', 'end_time', 'is_cancel')
        fields = '__all__'
        depth = 1
