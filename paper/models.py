from django.db import models


class Course(models.Model):
    slug = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=100, unique=True)
    duration_in_years = models.IntegerField(default=3)

    def __str__(self) -> str:
        return self.pk


class Semester(models.Model):
    SEMS_CHOICES = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6')
    )

    course = models.ForeignKey(
        Course, on_delete=models.CASCADE, related_name='sem_course')
    sem_number = models.CharField(
        choices=SEMS_CHOICES, default='5', max_length=1)

    # composite primary key
    class Meta:
        unique_together = (('course', 'sem_number'),)

    def __str__(self) -> str:
        return f"{self.course} - Sem {self.sem_number}"


class Paper(models.Model):
    PAPER_TYPES = (
        ('Theory', 'Theory'),
        ('Practical', 'Practical'),
        ('Sessional', 'Sessional')
    )

    code = models.CharField(max_length=50, primary_key=True)
    name = models.CharField(max_length=100, unique=True)
    course = models.ForeignKey(
        Course, on_delete=models.CASCADE, related_name='paper_course')
    semester = models.ForeignKey(
        Semester, on_delete=models.CASCADE, related_name='paper_semester')
    type = models.CharField(
        max_length=20, choices=PAPER_TYPES, default='Theory')
    credit = models.IntegerField(blank=True, null=True)

    def __str__(self) -> str:
        return f"{self.name} - {self.code}"
