from django.contrib import admin

from paper.models import Course, Paper, Semester


class SemAdmin(admin.ModelAdmin):
    list_display = ('id', 'sem_number', 'course')


admin.site.register(Course)
admin.site.register(Semester, SemAdmin)
admin.site.register(Paper)
