from django.shortcuts import render
from django.http.request import HttpHeaders
from django.shortcuts import render
from django.http import HttpResponse
import requests
import json
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework.decorators import api_view

from bcrecian.env import FRONTEND
# Create your views here.


def show_data(data):
    notification_ids = []
    body = data['target']
    print(json.dumps(body))
    res = requests.post(
        FRONTEND + '/api/auth/getnotificationids', data=json.dumps(body))
    print(res.json())
    if(res.ok):
        notification_ids = (res.json()['data'])
        send_notification(notification_ids, data['title'], data['body'])
    else:
        print('Failed to send data because of node api problem')


def send_notification(registration_ids, message_title, message_desc):
    fcm_api = "AAAAjbH_BBQ:APA91bEcyG48GaNyOEFSzJwpP_g1bW8aSRQMNurQQYqeIp5K7nCfD-XffY3RsIZLMqoeNll9AXJBh7x-UQ8WYrTYF6oBsNrAVvOmVcfixI30iiCdjXA7eXN-6jrEQD_A9XyOTEaAZEvy"
    url = "https://fcm.googleapis.com/fcm/send"

    headers = {
        "Content-Type": "application/json",
        "Authorization": 'key='+fcm_api}

    payload = {
        "registration_ids": registration_ids,
        "priority": "high",
        "notification": {
            "body": message_desc,
            "title": message_title,
            "icon": "https://image.freepik.com/free-vector/astronaut-sitting-planet-waving-hand-cartoon-vector-icon-illustration-science-technology-icon-concept-isolated-premium-vector-flat-cartoon-style_138676-3503.jpg",
            "click_action": FRONTEND
        }
    }

    result = requests.post(url,  data=json.dumps(payload), headers=headers)
    print(result.json())


def index(request):
    return render(request, 'index.html')


@csrf_exempt
def send(request):
    users = []
    notification_id_list = [
        'etf8XzKjFPkfywjPXLNO2b:APA91bExQHRHKvuG8E18NIC9XQNf1TjYJCQgZ1oM9sZ5uVNPPvll4onVw1C8QqYWDBOW5Prx63sUCAcM1wWb9J82MQz1ITsbHBCtvLfHcyIGzUWKLVXDbo2jChAKDsf5EDzt4iet8TNA']

    print(notification_id_list)
    resgistration = notification_id_list
    send_notification(
        resgistration, 'Code Keen added a new video', 'Code Keen new video alert')
    return HttpResponse("sent")


def show_firebase_js(request):
    data = 'importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js");' \
        'importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js"); ' \
        'var firebaseConfig = {' \
        'apiKey: "AIzaSyDC94VsmZh0YWkcrdeR_s8WZtg712DP5Bw",\
        authDomain: "django-notification-b0084.firebaseapp.com",\
        projectId: "django-notification-b0084",\
        storageBucket: "django-notification-b0084.appspot.com",\
        messagingSenderId: "608576668692",\
        appId: "1:608576668692:web:f6a710d7d3346ec8e1d301",\
        measurementId: "G-Y98CRFQ6Z6" };' \
        'firebase.initializeApp(firebaseConfig);' \
        'const messaging=firebase.messaging();' \
        'messaging.setBackgroundMessageHandler(function (payload) {' \
        '    console.log(payload);' \
        '    const notification=JSON.parse(payload);' \
        '    const notificationOption={' \
        '        body:notification.body,' \
        '        icon:notification.icon' \
        '    };' \
        '    return self.registration.showNotification(payload.notification.title,notificationOption);' \
        '});'

    return HttpResponse(data, content_type="text/javascript")
