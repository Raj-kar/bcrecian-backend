# Generated by Django 3.2.6 on 2021-12-06 15:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notification', '0011_notification_notification_target_roll'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notification',
            name='notification_id',
            field=models.CharField(default='notification0', editable=False, max_length=50),
        ),
        migrations.AlterField(
            model_name='notification',
            name='notification_target_roll',
            field=models.CharField(blank=True, default=None, max_length=1000, null=True),
        ),
    ]
