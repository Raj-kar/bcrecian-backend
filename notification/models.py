from django.db import models
from paper.models import Course

from django.dispatch import receiver
from django.db.models.signals import post_save
from .views import show_data
from datetime import datetime
# Create your models here.


SEMS_CHOICES = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
    ('6', '6')
)


SECTION_CHOICE = (
    ('A', 'A'),
    ('B', 'B')
)


class Notification(models.Model):
    notification_id = models.CharField(
        max_length=50, default=f"notification0", editable=False)
    notification_title = models.CharField(max_length=250)
    notification_body = models.TextField()
    notification_date = models.DateTimeField(auto_now=True)
    notification_target_stream = models.ForeignKey(
        Course, on_delete=models.CASCADE, null=True, blank=True)
    notification_target_semester = models.CharField(
        choices=SEMS_CHOICES, default=None, max_length=1, null=True, blank=True)
    notification_target_section = models.CharField(
        choices=SECTION_CHOICE, default=None, max_length=1, null=True, blank=True)

    notification_target_roll = models.CharField(
        default=None, max_length=1000, null=True, blank=True)

    class Meta:
        ordering = ['-notification_date']

    def save(self, *args, **kwargs):
        self.notification_id = f"notification{self.id}"
        print(self.notification_id)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.notification_title


@receiver(post_save, sender=Notification)
def _new_notification_receiver(sender, instance, created, **kwargs):
    data = {}
    data['title'] = instance.notification_title
    if created:
        data['title'] = instance.notification_title

    data['id'] = instance.id
    data['body'] = instance.notification_body

    data['target'] = []
    try:
        course = str(instance.notification_target_stream)
        data['target'].append(
            {'course': course})
    except Exception as e:
        pass
    try:
        data['target'].append(
            {'semester': instance.notification_target_semester})
    except Exception as e:
        pass
    try:
        data['target'].append(
            {'section': instance.notification_target_section})
    except Exception as e:
        pass
    try:
        data['target'].append({'roll': instance.notification_target_roll})
    except Exception as e:
        pass
    print(data)
    show_data(data)
