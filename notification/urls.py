from django.contrib import admin
from django.urls import path
from .views import show_firebase_js, index

urlpatterns = [
    path('', index),
    path('firebase-messaging-sw.js', show_firebase_js, name="show_firebase_js"),

]
