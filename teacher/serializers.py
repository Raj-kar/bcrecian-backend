from rest_framework import serializers

from .models import Teacher


class TeacherSerializer(serializers.ModelSerializer):

    class Meta:
        model = Teacher
        fields = ('name', 'slug', 'class_link', 'has_permanent_link')
