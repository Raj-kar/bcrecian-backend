from django.db import models


class Teacher(models.Model):
    name = models.CharField(max_length=100)
    slug = models.CharField(max_length=10)
    email = models.EmailField(blank=True, null=True)
    phone = models.IntegerField(blank=True, null=True)
    class_link = models.URLField(blank=True, null=True)
    has_permanent_link = models.BooleanField(default=True)

    def __str__(self) -> str:
        return self.name
