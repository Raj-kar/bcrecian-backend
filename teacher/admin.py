from django.contrib import admin
from .models import Teacher


class TeacherAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'email',
                    'class_link', 'has_permanent_link')


admin.site.register(Teacher, TeacherAdmin)
